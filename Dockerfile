FROM nginx:alpine
COPY ./nginx.conf /etc/nginx/conf.d
COPY /dist/BTBO /usr/share/nginx/html
EXPOSE 80
