import { Component, OnInit } from '@angular/core';
import { PieComponent } from 'src/app/d3/pie/pie.component';

import { ProductsService } from './../../services/products.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private productService: ProductsService) { }

  public products = [];
  public ttlAchats = 0;
  public ttlVentes = 0;
  public resume
  public allAchats = 0;
  public allVentes = 0;
  public allResume
  public prod
  public prodAchats = 0;
  public prodVentes = 0;
  public prodResume = 0
  fishs = [];
  seafood = [];
  crustaceans = [];
  listModifs = [];
  categories = [{
    name: "Poissons",
    id: 0
  }, {
    name: "Fruits de mer",
    id: 1
  }, {
    name: "Crustacées",
    id: 2
  }]
  showProduct = false;
  showCategory = false
  data = []

  ngOnInit(): void {
    this.getProducts()
    
  }

  getProducts() {
    this.productService.getProducts().subscribe(
      (response) => {
        this.products = response;
        this.filterByCategory();
      },
      (error) => {

      }
    );
  }

  filterByCategory() {
    for (var i = 0; i < this.products.length; i++) {
      if (this.products[i].sale) {
        this.products[i].price = this.products[i].price - (this.products[i].price * (this.products[i].discount / 100))
      }
      switch (this.products[i].category) {
        case 0:
          this.fishs.push(this.products[i]);
          break;
        case 1:
          this.seafood.push(this.products[i]);
          break;
        case 2:
          this.crustaceans.push(this.products[i]);
          break;
      }
    }
    this.getAllCA()
  }

  getCA() {
    for (var i = 0; i < this.data.length; i++) {

      this.productService.getCA(this.data[i].id).subscribe(
        (response) => {
          this.ttlAchats += Math.round(response.depense);
          this.ttlVentes += Math.round(response.gain);
        },
        (error) => {
          console.log(error)
        }
      );
    }
    this.sleep(500);
    this.setValue();

  }
  getAllCA() {
    for (var i = 0; i < this.products.length; i++) {

      this.productService.getCA(this.products[i].id).subscribe(
       
        (response) => {
          this.allAchats += parseFloat((response.depense).toFixed(2));
          this.allVentes += parseFloat((response.gain).toFixed(2));
        },
        (error) => {
          console.log(error)
        }
      );
      this.allResume = this.allAchats+this.allVentes
    }
    //this.allResume += (this.allAchats+this.allVentes).toFixed(2)
  }

  setValue() {
    this.ttlAchats = parseFloat((this.ttlAchats).toFixed(2))
    this.ttlVentes = parseFloat((this.ttlVentes).toFixed(2))
    this.resume = parseFloat((this.ttlVentes - this.ttlAchats).toFixed(2))
  }

  showCategoryView(idCat) {
    //réinitialisation du tableau des data
    this.data = []
    switch (idCat) {
      case 0:
        this.data = this.fishs
        break;
      case 1:
        this.data = this.seafood
        break;
      case 2:
        this.data = this.crustaceans
    }
    this.getCA()
    this.showProduct = false
    this.showCategory = true;
  }



  showProductView(idProd){
    this.prod = idProd
    this.productService.getCA(idProd).subscribe(
      (response) => {
        console.log(response)
        
        this.prodAchats=response.depense;
        this.prodVentes=response.gain;
        this.prodResume
      },
      (error) => {
        console.log(error)
      }
    );
      this.sleep(400)
      this.showCategory = false
    this.showProduct = true
  }

  sleep(milliseconds) {
    const date = Date.now();
    let currentDate = null;
    do {
      currentDate = Date.now();
    } while (currentDate - date < milliseconds);
  }
}
