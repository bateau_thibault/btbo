import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';

import {ApiService} from 'src/app/services/api.service'


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(public authService:AuthService,private apiService:ApiService) { }

  ngOnInit(): void {
    setInterval(() => {this.refresh()},60000)
  }
  

  refresh(){
    let refreshToken= {refresh: this.authService.getRefreshToken()}
    
    this.apiService.postTypeRequest('api/token/refresh/', refreshToken).subscribe((res: any) => {
      if(res.access){
        this.authService.setDataInLocalStorage('access_token', res.access)
      } 
    }, (error) => { 
      console.log(error) 
    }); 


  }

}
