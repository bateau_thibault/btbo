import { filter } from 'rxjs/operators';
import { Component, OnInit, Inject } from '@angular/core';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ProductsService } from 'src/app/services/products.service';
import { IProduct } from 'src/app/interface/product';

import { ProductModalComponent } from 'src/app/modals/product-modal/product-modal.component'

@Component({
  selector: 'app-details-product',
  templateUrl: './details-product.component.html',
  styleUrls: ['./details-product.component.css']
})
export class DetailsProductComponent implements OnInit {

  public products;
  public product;
  public showTable = false;
  public listModifs = []
  constructor(private productService: ProductsService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getProducts();

  }
  getProducts() {
    this.productService.getProducts().subscribe(
      (response) => {
        this.products = response;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  getProductDetails(idProduct) {

    //this.products.filter()
    this.productService.getProducts().subscribe(
      (response) => {
        //parcours de tous les produit et récupération du produit correspondant à IN
        for (var i = 0; i < response.length; i++) {
          if (response[i].id == idProduct) {
            this.product = response[i];
            if (this.product.sale) {
              this.product.price = this.product.price - (this.product.price * (this.product.discount / 100))
            }
            this.openDialog()
          }
        }
      },
      (error) => {
        console.log('Erreur ! : ' + error);
      }
    );
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ProductModalComponent, {
      /*width: '1000px',
      height: '200px',*/
      data: this.product
    });
    dialogRef.afterClosed().subscribe(result => {
      //récupération de dialogRef.listModifs et injection dans 
      //la listModifs du composant détail (celui-ci) a la fermeture du modal
      for(var i=0; i<result.data.length; i++){
      this.listModifs.push(result.data[i])}
    });
  }
  /*

*/

save() {
  //SI ON A EFFECTUÉ DES MODIFICATIONS
  if (this.listModifs.length > 0) {
    //POUR CHAQUE MODIF
    for (var i = 0; i < this.listModifs.length; i++) {
      //QUEL ESTR LA DIRECTIVE SAISIE PAR L'USER 
      switch (this.listModifs[i].directive) {
        case ("decrementQty"):
          //id,price,qty
          this.productService.sellProduct(this.listModifs[i].id, this.listModifs[i].value2, this.listModifs[i].value1).subscribe(
            (response) => {
            },
            (error) => {
              console.log(error)
            }
          );
          break;
        case ("incrementQty"):
          this.productService.addProduct(this.listModifs[i].id, this.listModifs[i].value2, this.listModifs[i].value1).subscribe(
            (response) => {
            },
            (error) => {
              console.log(error)
            }
          );
          break;
        case ("removeSale"):
          this.productService.removeSale(this.listModifs[i].id).subscribe(
            (response) => {
            },
            (error) => {
              console.log(error)
            }
          );
          break;
        case ("setSale"):
          this.productService.putOnsale(this.listModifs[i].id, this.listModifs[i].value1).subscribe(
            (response) => {
            },
            (error) => {
              console.log(error)
            }
          );
          break;
      }
    }
    alert('Les modifications on été pise en comptes.')
    this.products = [];
    this.getProducts()
    this.listModifs = []
  } else {
    alert('Aucune modification n\'a été saisie')
  }
}

removeModif(modif){
  //RETIRE L'EVT EN PARAM DE LA LIST DE MODIF
  this.listModifs= this.listModifs.filter(data => data != modif);
}
}