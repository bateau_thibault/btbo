import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  invalid: Boolean;
  constructor(
    private _api: ApiService,
    private _auth: AuthService,
    private router: Router,
    public fb: FormBuilder
  ) { }

  hide = true;
  loginError: any;
  
  ngOnInit(): void {
    this.form = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  login(){ 
    let b = this.form.value 
    this._api.postTypeRequest('/api/token/', b).subscribe((res: any) => { 
      if(res.access){ 
        this._auth.setDataInLocalStorage('access_token', res.access)
        this._auth.setDataInLocalStorage('refresh_token', res.refresh)
        this.router.navigate(['/productDetails'])
        this.loginError = false;
      } 
    }, err => { 
      console.log(err);
      this.invalid=true;
      alert(err.error.detail)
      this.loginError = true;
      this.getLoginErrorMessage();
    });
  }

  getLoginErrorMessage() {
    if (this.loginError == true){
      return 'Not a valid username or password'; 
    }
  }
}
