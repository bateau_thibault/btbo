
import { Component, NgModule, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ProductModalComponent } from 'src/app/modals/product-modal/product-modal.component';

import { ProductsService } from 'src/app/services/products.service';
import { MatFormFieldControl, MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

@Component({
  selector: 'app-category-product',
  templateUrl: './category-product.component.html',
  styleUrls: ['./category-product.component.css']
})

export class CategoryProductComponent implements OnInit {

  public product;

  constructor(private productService: ProductsService, public dialog: MatDialog) { }

  products = [];
  fishs = []
  seafood = []
  crustaceans = []
  listModifs = []

  ngOnInit(): void {
    this.getProducts()
  }

  getProducts() {
    this.productService.getProducts().subscribe(
      (response) => {
        this.products = response;
        this.filterByCategory()
      },
      (error) => {
        console.log('Erreur ! : ' + error);
      }
    );
  }

  filterByCategory() {
    for (var i = 0; i < this.products.length; i++) {
      if (this.products[i].sale) {
        this.products[i].price = this.products[i].price - (this.products[i].price * (this.products[i].discount / 100))
      }
      switch (this.products[i].category) {
        case 0:
          this.fishs.push(this.products[i]);
          break;
        case 1:
          this.seafood.push(this.products[i]);
          break;
        case 2:
          this.crustaceans.push(this.products[i]);
          break;
      }
    }
  }
  addEventToList(id, directive, name) {

    if (directive == "incrementQty" || directive == "decrementQty") {
      var value = (<HTMLInputElement>document.getElementById("qty_" + id.toString())).valueAsNumber;
      if (!Number.isInteger(value)) {
        return alert('Valeur non-accepté (not an int)');
      }
    }
    else if (directive == "setSale") {
      var value = (<HTMLInputElement>document.getElementById("sale_" + id.toString())).valueAsNumber;
      if (value > 100) {

        return alert('Valeur non-accepté (>100)');
      }
    }
    var evt = {
      "id": id,
      "directive": directive,
      "value": value,
      "name": name
    }
    this.listModifs.push(evt);
  }

  save() {
    //SI ON A EFFECTUÉ DES MODIFICATIONS
    if (this.listModifs.length > 0) {
      //POUR CHAQUE MODIF
      for (var i = 0; i < this.listModifs.length; i++) {
        //QUEL ESTR LA DIRECTIVE SAISIE PAR L'USER 
        switch (this.listModifs[i].directive) {
          case ("decrementQty"):
            //id,price,qty
            this.productService.sellProduct(this.listModifs[i].id, this.listModifs[i].value2, this.listModifs[i].value1).subscribe(
              (response) => {
              },
              (error) => {
                console.log(error)
              }
            );
            break;
          case ("incrementQty"):
            this.productService.addProduct(this.listModifs[i].id, this.listModifs[i].value2, this.listModifs[i].value1).subscribe(
              (response) => {
              },
              (error) => {
                console.log(error)
              }
            );
            break;
          case ("removeSale"):
            this.productService.removeSale(this.listModifs[i].id).subscribe(
              (response) => {
              },
              (error) => {
                console.log(error)
              }
            );
            break;
          case ("setSale"):
            this.productService.putOnsale(this.listModifs[i].id, this.listModifs[i].value1).subscribe(
              (response) => {
              },
              (error) => {
                console.log(error)
              }
            );
            break;
        }
      }
      alert('Les modifications on été pise en comptes.')
      this.products = [];
      this.seafood=[]
      this.crustaceans=[]
      this.fishs=[]
      this.getProducts()
      this.filterByCategory()
      this.listModifs = []
    } else {
      alert('Aucune modification n\'a été saisie')
    }
  }

  removeModif(modif) {
    //RETIRE L'EVT EN PARAM DE LA LIST DE MODIF
    this.listModifs = this.listModifs.filter(data => data != modif);
  }

  openDialog(product): void {
    const dialogRef = this.dialog.open(ProductModalComponent, {
      /*width: '1000px',
      height: '200px',*/
      data: product
    });
    dialogRef.afterClosed().subscribe(result => {
      //récupération de dialogRef.listModifs et injection dans 
      //la listModifs du composant détail (celui-ci) a la fermeture du modal
      for (var i = 0; i < result.data.length; i++) {
        this.listModifs.push(result.data[i])
      }
    });
  }


}
