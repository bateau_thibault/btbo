import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { DetailsProductComponent } from './components/details-product/details-product.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//MATERIAL UI IMPORT
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule, MatDialogRef} from '@angular/material/dialog'

import { InterceptorService } from './services/interceptor-service.service';

//IMPORT COMPONENT
import { CategoryProductComponent } from './components/category-product/category-product.component';
import { LoginComponent } from './components/login/login.component';
import { NavbarComponent } from './components/navbar/navbar.component';
//import { ProfileComponent } from './components/profile/profile.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ProductModalComponent } from './modals/product-modal/product-modal.component';
import { MatFormField } from '@angular/material/form-field';
import { BarComponent } from './d3/bar/bar.component';
import { PieComponent } from './d3/pie/pie.component';
import { ProductBarComponent } from './d3/product-bar/product-bar.component';

@NgModule({
  declarations: [
    AppComponent,
    DetailsProductComponent,
    CategoryProductComponent,
    LoginComponent,
    NavbarComponent,
    //ProfileComponent,
    DashboardComponent,
    ProductModalComponent,
    BarComponent,
    PieComponent,
    ProductBarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgSelectModule,
    ReactiveFormsModule,
    FormsModule,
    MatSelectModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule
  ],
  providers: [
    { 
      provide: HTTP_INTERCEPTORS, 
      useClass: InterceptorService, 
      multi: true 
    },
    {
      provide: MatDialogRef,
      useValue: {}
    }
  ],
  bootstrap: [AppComponent],
  entryComponents: [ProductModalComponent]
})
export class AppModule { }
