
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import { ProductsService } from 'src/app/services/products.service'


@Component({
  selector: 'app-product-modal',
  templateUrl: './product-modal.component.html',
  styleUrls: ['./product-modal.component.css']
})
export class ProductModalComponent implements OnInit {


  constructor(
    public dialogRef: MatDialogRef<ProductModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data, public productService:ProductsService) {}

    public showTable= true;
    public product= this.data;
    public listModifs = []

  onNoClick(): void {
    this.closeDialog();
  }
  closeDialog() {
    this.dialogRef.close({ event: 'close', data: this.listModifs });
  }

  ngOnInit() {
  }


  addProduct() {
    var price= (<HTMLInputElement>document.getElementById("buyPrice")).valueAsNumber;
    var qty = (<HTMLInputElement>document.getElementById("quantity")).valueAsNumber;
    this.productService.addProduct(this.product.id, price, qty).subscribe(
      (response) => {
        var oldQty = this.product.quantity
        this.product.quantity+= response.quantity_buy
        alert("La quantité du produit " + this.product.name +
          "à été mise à jour: " + oldQty + " => " + this.product.quantity);
      },
      (error) => {
        console.log(error)
      }
    );
  }

  sellProduct() {
    //Get quantity set in input #quantity
    var price= (<HTMLInputElement>document.getElementById("buyPrice")).valueAsNumber;
    if(!price){price=0}
    var qty = (<HTMLInputElement>document.getElementById("quantity")).valueAsNumber;
    this.productService.sellProduct(this.product.id, price, qty).subscribe(
      (response) => {
        var oldQty = this.product.quantity;
        this.product.quantity -= response.quantity_sold

        alert("La quantité du produit " + this.product.name +
          "à été mise à jour: " + oldQty + " -> " + this.product.quantity);
      },
      (error) => {
        console.log(error)
      }
    );
  }

  putOnsale(){

    var newSale = (<HTMLInputElement>document.getElementById("changeSale")).valueAsNumber;
    this.productService.putOnsale(this.product.id, newSale).subscribe(
      (response) => {
        var oldSale = this.product.discount;
        this.product = response
        alert("La promotion du produit " + this.product.name +
          "à été mise à jour: " + oldSale + " -> " + this.product.discount);
      },
      (error) => {
        console.log(error)
      });
  }
  removeSale(){
    this.productService.removeSale(this.product.id).subscribe(
      (response) => {
        this.product = response;
        alert("La promotion du produit " + this.product.name +
          " à été supprimé");
      },
      (error) => {
        console.log(error)
      });
  }

  addEventToList(id, directive, name) {

    if (directive == "incrementQty" || directive == "decrementQty"){
      //Qty achetée
      var value1 = (<HTMLInputElement>document.getElementById("quantity")).valueAsNumber;
      //Prix d'achat unitaire
      var value2 = (<HTMLInputElement>document.getElementById("buyPrice")).valueAsNumber;
      if(!value2){value2=0}
      if(!Number.isInteger(value1)){
        return alert('Valeur non-acceptée');
      }
    }
    else if (directive == "setSale") {
      var value1 = (<HTMLInputElement>document.getElementById("changeSale")).valueAsNumber;
      var value2 =0 
      if(value1>100){
        return alert('Valeur non-acceptée (>100)');
      }
    
    }
    else if (directive == "removeSale") {
      var value1 = 0
      var value2 = 0
    }
    var evt = {
      "id": id,
      "directive": directive,
      "value1": value1,
      "value2": value2,
      "name": name
    }
    this.listModifs.push(evt);
  }

}