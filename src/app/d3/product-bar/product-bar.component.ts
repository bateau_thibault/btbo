import { ProductsService } from 'src/app/services/products.service';
import { Component, OnInit, Input } from '@angular/core';
import * as d3 from 'd3';
@Component({
  selector: 'app-product-bar',
  templateUrl: './product-bar.component.html',
  styleUrls: ['./product-bar.component.css']
})
export class ProductBarComponent implements OnInit {

  @Input('prod') public prod: any;
  constructor(private productService: ProductsService) { }
  public Ymax = 250;
  public product
  public ventes: any;


  ngOnInit(): void {
    this.getCaProd();
  }

  getCaProd() {
    this.productService.getCA(this.prod).subscribe(
      (response) => {
        this.ventes = response.ventes

        this.createSvg();
        this.drawBars(this.ventes);
      },
      (error) => {
        //          prod.quantity_sold=0
      }
    );


  }
  sleep = function (ms) {
    let now = Date.now(),
      end = now + ms;
    while (now < end) {
      now = Date.now();
    }
  };



  /*private data = [
    {"Framework": "Vue", "Stars": "166443", "Released": "2014"},
    {"Framework": "React", "Stars": "150793", "Released": "2013"},
    {"Framework": "Angular", "Stars": "62342", "Released": "2016"},
    {"Framework": "Backbone", "Stars": "27647", "Released": "2010"},
    {"Framework": "Ember", "Stars": "21471", "Released": "2011"},
  ];*/
  private svg;
  private margin = 30;
  private width = 375 - (this.margin * 2);
  private height = 300 - (this.margin * 2);


  private createSvg(): void {
    this.svg = d3.select("figure#product-bar")
      .append("svg")
      .attr("width", this.width + (this.margin * 2))
      .attr("height", this.height + (this.margin * 2))
      .append("g")
      .attr("transform", "translate(" + this.margin + "," + this.margin + ")");
  }

  private drawBars(data: any[]): void {
    // Create the X-axis band scale
    const x = d3.scaleBand()
      .range([0, this.width])
      .domain(data.map(d => d.date))
      .padding(0.2);

    // Draw the X-axis on the DOM
    this.svg.append("g")
      .attr("transform", "translate(0," + this.height + ")")
      .call(d3.axisBottom(x))
      .selectAll("text")
      .attr("transform", "translate(-30,0)rotate(-45)")
      .style("text-anchor", "end");

    // Create the Y-axis band scale
    const y = d3.scaleLinear()
      .domain([0, this.Ymax])
      .range([this.height, 0]);

    // Draw the Y-axis on the DOM
    this.svg.append("g")
      .call(d3.axisLeft(y));




    // Create and fill the bars
    this.svg.selectAll("bars")
      .data(data)
      .enter()
      .append("rect")
      .attr("x", d => x(d.name))
      .attr("y", d => y(d.quantity_sold))
      .attr("width", x.bandwidth())
      .attr("height", (d) => this.height - y(d.quantity_sold))
      .attr("fill", "#d04a35");
  }
}
