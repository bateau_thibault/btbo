import { Component, Input, OnInit } from '@angular/core';
import * as d3 from 'd3';

@Component({
  selector: 'app-pie',
  templateUrl: './pie.component.html',
  styleUrls: ['./pie.component.css']
})
export class PieComponent implements OnInit {

  @Input('products') public products: any;
  constructor() { }

  private data = [
    {name: "Poissons", totalSold: 0},
    {name: "Fruits de Mer", totalSold: 0},
    {name: "Crustacés", totalSold: 0},
  ];

  ngOnInit(): void {
    this.totalSaleByCategory();
    this.createSvg();
    this.createColors();
    this.drawChart();
  }
  
  private svg;
  private margin = 50;
  private width = 250;
  private height = 300;
  // The radius of the pie chart is half the smallest side

  private radius = Math.min(this.width, this.height) / 2 - this.margin;
  private colors;

  private createSvg(): void {
    this.svg = d3.select("figure#pie")
    .append("svg")
    .attr("width", this.width)
    .attr("height", this.height)
    .append("g")
    .attr(
      "transform",
      "translate(" + this.width / 2 + "," + this.height / 2 + ")"
    );
  }

  private createColors(): void {
    this.colors = d3.scaleOrdinal()
    .domain(this.data.map(d => d.name.toString()))
    .range(["#D46A6A", "#801515", "#550000"]);
  }

  private drawChart(): void {
    // Compute the position of each group on the pie:
    const pie = d3.pie<any>().value((d: any) => Number(d.totalSold));

    // Build the pie chart
    this.svg
    .selectAll('pieces')
    .data(pie(this.data))
    .enter()
    .append('path')
    .attr('d', d3.arc()
      .innerRadius(0)
      .outerRadius(this.radius)
    )
    .attr('fill', (d, i) => (this.colors(i)))
    .attr("stroke", "#121926")
    .style("stroke-width", "1px");

    // Add labels
    const labelLocation = d3.arc()
    .innerRadius(100)
    .outerRadius(this.radius);

    this.svg
    .selectAll('pieces')
    .data(pie(this.data))
    .enter()
    .append('text')
    .text(d => d.data.name)
    .attr("transform", d => "translate(" + labelLocation.centroid(d) + ")")
    .style("text-anchor", "middle")
    .style("font-size", 15);
  }

  totalSaleByCategory() {
    for (var i = 0; i < this.products.length; i++) {
      switch (this.products[i].category) {
        case 0:
          this.data[0].totalSold += this.products[i].quantity_sold;
          break;
        case 1:
          this.data[1].totalSold += this.products[i].quantity_sold;
          break;
        case 2:
          this.data[2].totalSold += this.products[i].quantity_sold;
          break;
      }
    }
  }
}
