import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  env = environment;
  constructor(public httpClient:HttpClient) { }

  getProducts(){
    return this.httpClient.get<any>(this.env.apiUrl+"/products/");
  }
  incrementsProduct(id,qty){ //Brut déprécié
    return this.httpClient.get<any>(this.env.apiUrl+"/incrementStock/"+id+"/"+qty+"/");
  }
  decrementProduct(id,qty)//Brut déprécié
  {
    return this.httpClient.get<any>(this.env.apiUrl+"/decrementStock/"+id+"/"+qty+"/");
  }
  putOnsale(id,newSale){
    return this.httpClient.get<any>(this.env.apiUrl+"/putonsale/"+id+"/"+newSale+"/");
  }
  removeSale(id){
    return this.httpClient.get<any>(this.env.apiUrl+"/removesale/"+id+"/");
  }
  addProduct(id, price, qty){
    var time = Math.round(new Date().getTime() / 1000); //Get current timestamp (second)
    return this.httpClient.get<any>(this.env.apiUrl+"/buyProduct/"+id+"/"+price+"/"+qty+"/"+time+"/");
  }
  sellProduct(id, price, qty){
    var time = Math.round(new Date().getTime() / 1000); //Get current timestamp (second)
    return this.httpClient.get<any>(this.env.apiUrl+"/sellProduct/"+id+"/"+price+"/"+qty+"/"+time+"/");
  }
  getCA(id){
    return this.httpClient.get<any>(this.env.apiUrl+"/getCA/"+id);
  }
}