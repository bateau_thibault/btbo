import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, Route } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { InterceptorService } from './interceptor-service.service'
@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {
    constructor(
      private _authService: AuthService,
      private _router: Router,
      private _refreshService: InterceptorService
    ) { }
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this._authService.getToken()) {
      return true;
    } else {
      var refresh = this._authService.getRefreshToken()

    }

    // navigate to login page
    this._router.navigate(['/login']);
    // you can save redirect url so after authing we can move them back to the page they requested
    return false;
  }
}