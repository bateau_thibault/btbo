import { Injectable } from '@angular/core'; 
import { HttpClient, HttpHeaders } from '@angular/common/http'; 
import { map } from 'rxjs/operators'; 
 
import { environment } from 'src/environments/environment';

@Injectable({ 
  providedIn: 'root' 
}) 
export class ApiService { 
 
  env = environment;

  constructor(private httpClient: HttpClient) { } 
 
  getTypeRequest(url) { 
    return this.httpClient.get(this.env.apiUrl+url).pipe(map(res => { 
      return res; 
    })); 
  } 
 
  postTypeRequest(url, payload) { 
    return this.httpClient.post(this.env.apiUrl+url, payload).pipe(map(res => { 
      return res; 
    })); 
  } 
 
  putTypeRequest(url, payload) { 
    return this.httpClient.put(this.env.apiUrl+url, payload).pipe(map(res => { 
      return res; 
    })) 
  }   
}