import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {AppComponent} from './app.component'

//COMPONENTS CALLED
import {DetailsProductComponent} from './components/details-product/details-product.component';
import {CategoryProductComponent } from './components/category-product/category-product.component'
import { LoginComponent } from './components/login/login.component';
import { AuthGuardService } from './services/auth-guard.service';
import { ProfileComponent } from './components/profile/profile.component';
import { DashboardComponent }from './components/dashboard/dashboard.component'

const routes: Routes = [{ path: 'home', component: AppComponent, canActivate: [AuthGuardService]},
{ path: '', redirectTo: 'login', pathMatch: 'full' }, 
{path: 'productDetails', component: DetailsProductComponent },
{ path: 'productCategory', component: CategoryProductComponent },
{ path: 'profile', component: ProfileComponent},
{path: 'login', component: LoginComponent},
{path: 'dashboard', component: DashboardComponent}];

@NgModule(
  {
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
